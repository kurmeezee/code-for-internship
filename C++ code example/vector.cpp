#include "vector.h"
#include "algorithm"
Vector::Iterator::Iterator(ValueType* pointer) {
    ptr_ = pointer;
}
Vector::Iterator::Iterator() {
}

Vector::ValueType& Vector::Iterator::operator*() const {
    return *ptr_;
}
Vector::ValueType* Vector::Iterator::operator->() const {
    return ptr_;
}
Vector::Iterator& Vector::Iterator::operator=(Vector::Iterator other) {
    ptr_ = other.ptr_;
    return *this;
}
Vector::Iterator& Vector::Iterator::operator++() {
    ptr_++;
    return *this;
}
Vector::Iterator Vector::Iterator::operator++(int i) {
    Iterator it(ptr_);
    ptr_++;
    return it;
}
Vector::Iterator& Vector::Iterator::operator--() {
    ptr_--;
    return *this;
}
Vector::Iterator Vector::Iterator::operator--(int i) {
    Iterator it(ptr_);
    ptr_--;
    return it;
}
Vector::Iterator Vector::Iterator::operator+(DifferenceType shift) {
    Vector::Iterator answer = *this;
    if (shift > 0) {
        for (int64_t i = 0; i < static_cast<int>(shift); i++) {
            answer++;
        }
    } else {
        for (int64_t i = 0; i < -static_cast<int>(shift); i++) {
            answer--;
        }
    }
    return answer;
}
Vector::DifferenceType Vector::Iterator::operator-(Iterator other) {
    Vector::DifferenceType answer = 0;
    while (1) {
        if (other.ptr_ + answer == ptr_) {
            return answer;
        }
        if (other.ptr_ == ptr_ + answer) {
            return -answer;
        }
        answer++;
    }
}
Vector::DifferenceType Vector::Iterator::operator-(Iterator other) const {
    Vector::DifferenceType answer = 0;
    while (1) {
        if (other.ptr_ + answer == ptr_) {
            return answer;
        }
        if (other.ptr_ == ptr_ + answer) {
            return -answer;
        }
        answer++;
    }
}
Vector::Iterator& Vector::Iterator::operator+=(DifferenceType shift) {
    for (int64_t i = 0; i < static_cast<int>(shift); i++) {
        ptr_++;
    }
    return *this;
}
Vector::Iterator& Vector::Iterator::operator-=(DifferenceType shift) {
    for (int64_t i = 0; i < static_cast<int>(shift); i++) {
        ptr_--;
    }
    return *this;
}

bool Vector::Iterator::operator==(const Iterator& other) const {
    return other.ptr_ == this->ptr_;
}
bool Vector::Iterator::operator!=(const Iterator& other) const {
    return !(other == *this);
}
std::strong_ordering Vector::Iterator::operator<=>(const Iterator& other) const {
    if (*this - other > 0) {
        return std::strong_ordering::greater;
    } else if (*this - other < 0) {
        return std::strong_ordering::less;
    } else {
        return std::strong_ordering::equal;
    }
}

Vector::Vector() {
    size_ = 0;
    capacity_ = 0;
    storage_ = nullptr;
}
Vector::Vector(size_t size) {
    size_ = size;
    capacity_ = size;
    storage_ = new int[size];
    for (size_t t = 0; t < size_; t++) {
        storage_[t] = 0;
    }
}
Vector::Vector(std::initializer_list<ValueType> list) {
    size_ = list.size();
    capacity_ = list.size();
    storage_ = new int[list.size()];
    size_t i = 0;
    for (auto it = list.begin(); it != list.end(); it++) {
        storage_[i] = *it;
        i++;
    }
}
Vector::Vector(const Vector& other) {
    size_ = other.size_;
    capacity_ = other.capacity_;
    storage_ = other.storage_;
}
Vector& Vector::operator=(const Vector& other) {
    size_ = other.size_;
    capacity_ = other.capacity_;
    storage_ = other.storage_;
    return *this;
}
Vector::~Vector() {
    delete[] storage_;
    storage_ = nullptr;
}
Vector::SizeType Vector::Size() const {
    return size_;
}
Vector::SizeType Vector::Capacity() const {
    return capacity_;
}
const Vector::ValueType* Vector::Data() const {
    return storage_;
}
Vector::ValueType& Vector::operator[](size_t position) {
    return storage_[position];
}
Vector::ValueType Vector::operator[](size_t position) const {
    return storage_[position];
}
bool Vector::operator==(const Vector& other) const {
    if (size_ == other.size_) {
        for (size_t t = 0; t < size_; t++) {
            if (storage_[t] != other.storage_[t]) {
                return false;
            }
            return true;
        }
    }
    return false;
}
bool Vector::operator!=(const Vector& other) const {
    return !(*this == other);
}
std::strong_ordering Vector::operator<=>(const Vector& other) const {
    for (size_t i = 0; i < std::min(other.size_, size_); i++) {
        if (other.storage_[i] > storage_[i]) {
            return std::strong_ordering::less;
        } else if (other.storage_[i] < storage_[i]) {
            return std::strong_ordering::greater;
        }
    }
    if (other.size_ == size_) {
        return std::strong_ordering::equal;
    } else if (other.size_ > size_) {
        return std::strong_ordering::less;
    } else {
        return std::strong_ordering::greater;
    }
}
void Vector::Reserve(SizeType new_capacity) {
    if (new_capacity > capacity_) {
        int* new_int_ptr = new int[new_capacity];
        for (size_t t = 0; t < size_; t++) {
            new_int_ptr[t] = storage_[t];
        }
        delete[] storage_;
        capacity_ = new_capacity;
        storage_ = new_int_ptr;
    }
}
void Vector::Clear() {
    delete[] storage_;
    storage_ = nullptr;
    size_ = 0;
}
void Vector::PushBack(const ValueType& new_element) {
    if (storage_ == nullptr and capacity_ > 0) {
        storage_ = new int[capacity_];
        storage_[0] = new_element;
        size_ = 1;
    } else if (capacity_ == 0) {
        capacity_++;
        storage_ = new int[capacity_];
        storage_[0] = new_element;
        size_ = 1;
    } else if (size_ == capacity_) {
        int* new_storage = new int[2 * capacity_];
        for (size_t i = 0; i < size_; i++) {
            new_storage[i] = storage_[i];
        }
        delete[] storage_;
        new_storage[size_] = new_element;
        storage_ = new_storage;
        size_++;
        capacity_ *= 2;
    } else {
        storage_[size_] = new_element;
        size_++;
    }
}
void Vector::PopBack() {
    if (size_ != 0) {
        size_--;
    }
}
void Vector::Swap(Vector& other) {
    int* a = storage_;
    storage_ = other.storage_;
    other.storage_ = a;

    size_t t = size_;
    size_ = other.size_;
    other.size_ = t;

    size_t c = capacity_;
    capacity_ = other.capacity_;
    other.capacity_ = c;
}

Vector::Iterator Vector::Begin() {
    Iterator it(storage_);
    return it;
}
Vector::Iterator Vector::End() {
    Iterator it(storage_ + size_);
    return it;
}
Vector::Iterator Vector::begin() {
    Iterator it(storage_);
    return it;
};  // NOLINT
Vector::Iterator Vector::end() {
    Iterator it(storage_ + size_);
    return it;
};  // NOLINT